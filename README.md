## Demo Project

### Build and run

####Build
```bash
cd /app/path
bower install
mvn clean package -DskipTests=true
```

#### Install For Linux OS
https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html

```bash
# Create user without home directory
useradd -M appuser

# Lock the account to prevent logging in
usermod -L appuser

# Change app ownership & permission
chown appuser:appuser /app/path/target/demo-0.0.1.jar
chmod 500 /app/path/target/demo-0.0.1.jar

# Install in service diretory
sudo ln -s /app/path/target/demo-0.0.1.jar /etc/init.d/demo

# Auto start
update-rc.d demo defaults 1

# Create startup script config
echo "LOG_FOLDER=/var/log/demo" > /app/path/target/demo-0.0.1.config
```

### Configurations

Open the `application.properties` file and set your own configurations for the
database connection.