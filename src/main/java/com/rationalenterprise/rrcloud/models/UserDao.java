package com.rationalenterprise.rrcloud.models;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by azhao on 5/17/2016. All Rights Reserved 2016.
 * <p>
 * A DAO for the User is simply created by extending the CrudRepository
 * interface provided by spring. The following methods are some of the ones
 * available from such interface: save, delete, deleteAll, findOne and findAll.
 * The magic is that such methods must not be implemented, and moreover it is
 * possible create new query methods working only by defining their signature!
 */
@Transactional
public interface UserDao extends CrudRepository<User, Long> {
    /**
     * Find user by email.
     *
     * @param email The email to find.
     * @return The user with matching email
     */
    User findByEmail(String email);

    /**
     * Find user by name.
     *
     * @param username The username to find.
     * @return The user with matching username.
     */
    User findByUsername(String username);

    /**
     * Find users by role.
     *
     * @param role The role to find.
     * @return The users with matching role.
     */
    List<User> findByRole(String role);
}
